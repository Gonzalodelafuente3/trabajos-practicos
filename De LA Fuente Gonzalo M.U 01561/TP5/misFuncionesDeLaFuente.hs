
{--1--}
potencia :: Integer -> Integer -> Integer
potencia x 0 = 1
potencia x n = x * potencia x (n - 1)

{--2--}
celsiusAFerenheit :: Double -> Double
celsiusAFerenheit x = x * 1.800 + 32

{--3--}
concatenaLista :: [a] -> [a] -> [a]
concatenaLista [] ys = ys
concatenaLista (x : xs) ys = x : (concatenaLista xs ys)

{--4--}
sumarCifras :: Integer -> Integer
sumarCifras x
  | x < 10 = x
  | otherwise = (mod x 10) + sumarCifras (div x 10)

{--5--}
divisores x = [y | y <- [1 .. x -1], mod x y == 0]

esNumeroPerfecto :: Integer -> Bool
esNumeroPerfecto x = x == sum (divisores x)

{--6--}
numerosPerfectosMenoresA :: Integer -> [Integer]
numerosPerfectosMenoresA x = [n | n <- [1 .. x], esNumeroPerfecto n]

{--7--}
sumatoria :: Integer -> Integer -> Integer
sumatoria x y
  | x < y = x + sumatoria (x + 1) y
  | x == y = y
  | otherwise = error "los Numeros fueron mal ingresados.vuelva a intentar"

{--8--}
maximoDe2 :: Integer -> Integer -> Integer
maximoDe2 x y = if x > y then x else y

{--la diferencia con la funcion max es que esta puede recibir cualquier valor y va a retonar com el mismo tipo
recibido el mayor de ambos--}
{--9--}
maximoDe3 :: Integer -> Integer -> Integer -> Integer
maximoDe3 x y z = maximoDe2 x (maximoDe2 y z)

{--12--}
infixr 3 !&&

(!&&) :: Bool -> Bool -> Bool
True !&& True = False
_ !&& _ = True

{--14--}
promedio :: Double -> Double -> Double
promedio x y = x / y

sumarNotas :: [Double] -> Double
sumarNotas x = sum [y | y <- x]

mostrarNotas :: [Double] -> [Double]
mostrarNotas x = [y | y <- x]

contarNotas :: [Double] -> Double
contarNotas x = sum [1 | y <- x]

{--15--}
data Futbolista = UnFutbolista {nombre :: String, posiciones :: [Integer], goles :: Integer} deriving (Show)

hizoMasDe10Goles (UnFutbolista nombre posiciones goles)
  | goles > 10 = True
  | otherwise = False

juegoMasDeUnaPosicion (UnFutbolista nombre posiciones goles) = length posiciones > 1

agregarNuevaPosicion nuevaPosicion posiciones
  | not (elem nuevaPosicion posiciones) && between 1 11 nuevaPosicion = nuevaPosicion : posiciones
  | otherwise = posiciones

between x y z = x <= z && z <= y

agregarGol :: Integer -> Futbolista -> Futbolista
agregarGol nuevosGoles (UnFutbolista nombre posiciones goles) = UnFutbolista nombre posiciones (goles + nuevosGoles)

{--10--}
listaInversa :: [Integer] -> [Integer]
listaInversa [] = []
listaInversa (x : xs) = listaInversa xs ++ [x]

{--11--}
listaEnteros :: [Integer] -> [Integer] -> [Integer]

listaEnteros xs ys = [x* y | x <- xs, y <- filter (even) ys ]

--productoPares x y = [z*c | c <- x , z<- y, mod z 2 == 0 ]

{--13--}
notPar :: Integer -> Bool
notPar x = (not (even x))
-- :t not . even

{--16--}
dispersion :: Integer -> Integer -> Integer -> Integer
dispersion x y z = maximoDe3 x y z - minimoDe3 x y z

minimoDe3 :: Integer -> Integer -> Integer -> Integer
minimoDe3 x y z = minimoDe2 x (minimoDe2 y z)

minimoDe2 :: Integer -> Integer -> Integer
minimoDe2 x y = if x < y then x else y

{--18--}
maximoEntero :: [Integer] -> Integer
maximoEntero [] = error "No hay valor maximo"
maximoEntero [x] = x
maximoEntero (x : y : xs) =
  if x > y
    then maximoEntero (x : xs)
    else maximoEntero (y : xs)

{--
maximoEntero :: [Integer] -> Integer
maximoEntero [] = error "No hay valor maximo"
maximoEntero [x] = x
maximoEntero (x:xs)
   |x < maximoEntero xs = maximoEntero xs
   |x >= maximoEntero xs = x 
--}

{--17--}
duplaMayorAMenor :: Integer -> Integer -> (Integer, Integer)
duplaMayorAMenor x y
  | x >= y = (x, y)
  | y > x = (y, x)
{--
parOrdenado a b = (max a b , min a b)
--}
--19
data Figura
  = Triangulo {lado1 :: Double, lado2 :: Double, lado3 :: Double}
  | Cuadrado {lado1 :: Double}
  | Rectangulo {base :: Double, altura :: Double}
  | Circulo {diametro :: Double}

calcularPerimetroTriangulo (Triangulo lado1 lado2 lado3) = lado1 + lado2 + lado3

calcularPerimetroCuadrado (Cuadrado lado1) = lado1 * 4

calcularPerimetroRectangulo (Rectangulo base altura) = 2 * base + 2 * altura

calcularPerimetroCirculo (Circulo diametro) = 3.14 * diametro
{--
calcularPerimetro :: Figura -> Double
calcularPerimetro (Triangulo lado1 lado2 lado3) = lado1 + lado2 + lado3
calcularPerimetro (Cuadrado lado1) = lado1 * 4
calcularPerimetro (Rectangulo base altura) = 2 * base + 2 * altura
calcularPerimetro (Circulo diametro) = pi * diametro

--}
--Falta mensaje de error
--20
data Temp
  = Centigrados {gradosCentigrados :: Double}
  | Farenheit {gradosFarenheit :: Double}
  deriving Show

farenheitAcentigrados :: Temp -> Double
farenheitAcentigrados (Farenheit gradosFarenheit) = (gradosFarenheit -32) / 1.8

centigradosAFarenheit :: Temp -> Double
centigradosAFarenheit (Centigrados gradosCentigrados) = gradosCentigrados * 1.8 + 32

congelamiento:: Temp -> Bool 

congelamiento(Centigrados gradosCentigrados)=(gradosCentigrados<=0.0)

congelamiento(Farenheit gradosFarenheit )=(gradosFarenheit<=32)
--21
data Clave = Clave {tipo :: Integer , numero :: Integer, verificador :: Integer} deriving Show

--20 45632987 1 persona
constante = [5,4,3,2,7,6,5,4,3,2]

{--hacerLista (Clave tipo numero verificador)
hacerLista 

validar (Clave tipo numero verificador)
validar(tipo) ([])=[]

validar([]) (numero)=[]

validar(tipo)  (numero) = x*y :validar  xs ys--}

valor2 :: Integer -> Integer 
valor2 x = mod x 11

valor3 :: Integer -> Integer 
valor3 x = 11 - x 
--20-17254359-7.


aDigito :: Integer -> [Integer]
aDigito 0 = []
aDigito x = aDigito (x `div` 10) ++ [mod x 10]