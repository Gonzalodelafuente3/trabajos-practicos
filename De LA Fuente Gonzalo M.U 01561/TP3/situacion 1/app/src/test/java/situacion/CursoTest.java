package situacion;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.Assume.assumeFalse;
import static org.junit.Assume.assumeTrue;
public class CursoTest {
    
    /**
     *
     */
    
    @Test
    public void ingresarUnEstudiante(){
        Curso curso1 =new Curso("calculo",3333);
        Estudiante estudiante1 = new Estudiante("lucas","guzman", 19, 123, "capital",4515);

        curso1.agregarEstudiante(estudiante1);
        assertEquals(1,curso1.cantidadDeEstudiantesInscriptos());
    }
    @Test
    public void ingresarMasDeUnEstudiante(){
        Curso curso1 =new Curso("calculo",3333);
        Estudiante estudiante1 = new Estudiante("lucas","guzman", 19, 123, "capital",5616);
        Estudiante estudiante2 = new Estudiante("martin","guzman", 20, 465, "capital",464686);
        Estudiante estudiante3 = new Estudiante("lucas","ceballo", 26, 158, "capital",849);
        curso1.agregarEstudiante(estudiante1);
        curso1.agregarEstudiante(estudiante2);
        curso1.agregarEstudiante(estudiante3);
        assertEquals(3,curso1.cantidadDeEstudiantesInscriptos());
    }
    @Test
    public void resetearNotas(){
        Curso curso1 = new Curso("python", 3310);
        Estudiante estudiante1 = new Estudiante("lucas","guzman", 19, 123, "capital",6318); 
        estudiante1.setCalificacion(8);
        curso1.agregarEstudiante(estudiante1);
        curso1.resetearNotas();
        assertEquals(0,estudiante1.getCalificacion(),0);
    }
    @Test
    public void estudianteConNotaDiez(){
        Curso curso1 = new Curso("python", 3310);
        Estudiante estudiante1 = new Estudiante("lucas","guzman", 19, 123, "capital",68);
        Estudiante estudiante2 = new Estudiante("martin","guzman", 20, 465, "capital",618);
        estudiante1.setCalificacion(10);
        estudiante2.setCalificacion(8);
        curso1.agregarEstudiante(estudiante1);
        curso1.agregarEstudiante(estudiante2);
        assumeTrue(curso1.existeEstudianteConNotaDiez());
    }
    @Test
    public void noHayEstudianteConNotaDiez(){
        Curso curso1 = new Curso("python", 3310);
        Estudiante estudiante1 = new Estudiante("lucas","guzman", 19, 123, "capital",94);
        Estudiante estudiante2 = new Estudiante("martin","guzman", 20, 465, "capital",95);
        estudiante1.setCalificacion(5);
        estudiante2.setCalificacion(8);
        curso1.agregarEstudiante(estudiante1);
        curso1.agregarEstudiante(estudiante2);
        assumeFalse(curso1.existeEstudianteConNotaDiez());

    }
    @Test
    public void cantidadDeAlumnosAprobados(){
        Curso curso1 =new Curso("calculo",3333);
        Estudiante estudiante1 = new Estudiante("lucas","guzman", 19, 123, "capital",685);
        Estudiante estudiante2 = new Estudiante("martin","guzman", 20, 465, "capital",648);
        Estudiante estudiante3 = new Estudiante("lucas","ceballo", 26, 158, "capital",560);
        estudiante1.setCalificacion(8);
        estudiante2.setCalificacion(8);
        estudiante3.setCalificacion(2);

  
        curso1.agregarEstudiante(estudiante1);
        curso1.agregarEstudiante(estudiante2);
        curso1.agregarEstudiante(estudiante3);

        assertEquals(2,curso1.cantidadDeAlumnosAprobados());

    }
    @Test 
    public void porcentajeDeAprobados(){
        Curso curso1 =new Curso("lengua",3333);
        Estudiante estudiante1 = new Estudiante("lucas","guzman", 19, 123, "capital",16);
        Estudiante estudiante2 = new Estudiante("martin","guzman", 20, 465, "capital",8);
        Estudiante estudiante3 = new Estudiante("lucas","ceballo", 26, 158, "capital",648);
        Estudiante estudiante4 = new Estudiante("milton","zeballo", 33, 166, "capital",516);
        Estudiante estudiante5 = new Estudiante("lautaro","contrera", 18, 151, "capital",130);
        estudiante1.setCalificacion(8);
        estudiante2.setCalificacion(8);
        estudiante3.setCalificacion(2);
        estudiante4.setCalificacion(10);
        estudiante5.setCalificacion(2);

        curso1.agregarEstudiante(estudiante1);
        curso1.agregarEstudiante(estudiante2);
        curso1.agregarEstudiante(estudiante3);
        curso1.agregarEstudiante(estudiante4);
        curso1.agregarEstudiante(estudiante5);
        assertEquals(60,00,curso1.porcentajeDeAprobados());
    }
    @Test
    public void promedioDeCalificaciones(){
        Curso curso1 =new Curso("lengua",3333);
        Estudiante estudiante1 = new Estudiante("mateo","guzman", 19, 123, "capital",306);
        Estudiante estudiante2 = new Estudiante("martin","guzman", 20, 465, "chumbicha",516);
        Estudiante estudiante3 = new Estudiante("lucas","ceballo", 26, 158, "capital",641);
        Estudiante estudiante4 = new Estudiante("milton","zeballo", 33, 166, "capital",68);
        Estudiante estudiante5 = new Estudiante("lautaro","contrera", 18, 151, "valle viejo",646);
        estudiante1.setCalificacion(8);
        estudiante2.setCalificacion(8);
        estudiante3.setCalificacion(2);
        estudiante4.setCalificacion(10);
        estudiante5.setCalificacion(2);

        curso1.agregarEstudiante(estudiante1);
        curso1.agregarEstudiante(estudiante2);
        curso1.agregarEstudiante(estudiante3);
        curso1.agregarEstudiante(estudiante4);
        curso1.agregarEstudiante(estudiante5);
        assertEquals(6,00,curso1.promedioDeCalificaciones());


    }
    @Test
    public void esUnDesastre(){
        Curso curso1 =new Curso("python",3333);
        Estudiante estudiante1 = new Estudiante("mateo","guzman", 19, 123, "capital",68);
        Estudiante estudiante2 = new Estudiante("martin","guzman", 20, 465, "chumbicha",166);
        Estudiante estudiante3 = new Estudiante("lucas","ceballo", 26, 158, "capital",035);

        estudiante1.setCalificacion(1);
        estudiante2.setCalificacion(3);
        estudiante3.setCalificacion(2);
        curso1.agregarEstudiante(estudiante1);
        curso1.agregarEstudiante(estudiante2);
        curso1.agregarEstudiante(estudiante3);
        assertTrue(curso1.unDesastre());
    }
    public void AlmenosUnAprobado(){
        Curso curso1 =new Curso("python",3333);
        Estudiante estudiante1 = new Estudiante("mateo","guzman", 19, 123, "capital",688);
        Estudiante estudiante2 = new Estudiante("martin","guzman", 20, 465, "chumbicha",28);
        Estudiante estudiante3 = new Estudiante("lucas","ceballo", 26, 158, "capital",68610);

        estudiante1.setCalificacion(1);
        estudiante2.setCalificacion(3);
        estudiante3.setCalificacion(9);
        curso1.agregarEstudiante(estudiante1);
        curso1.agregarEstudiante(estudiante2);
        curso1.agregarEstudiante(estudiante3);
        assumeFalse(curso1.unDesastre());
    }
    @Test 
    public void NoExisteEstudiante(){
        Curso curso1 =new Curso("python",3333);
        Estudiante estudiante1 = new Estudiante("mateo","guzman", 19, 123, "capital",688);
        Estudiante estudiante2 = new Estudiante("martin","guzman", 20, 465, "chumbicha",28);
        curso1.agregarEstudiante(estudiante1);
        curso1.agregarEstudiante(estudiante2);
        assertTrue(curso1.existeEstudiante(28));
    }
    @Test 
    public void existeEstudiante(){
        Curso curso1 =new Curso("python",3333);
        Estudiante estudiante1 = new Estudiante("mateo","guzman", 19, 123, "capital",688);
        Estudiante estudiante2 = new Estudiante("martin","guzman", 20, 465, "chumbicha",28);
        curso1.agregarEstudiante(estudiante1);
        curso1.agregarEstudiante(estudiante2);
        assertFalse(curso1.existeEstudiante(03));
    }
    @Test 
    public void existeEstudianteLlamado(){
        Curso curso1 =new Curso("python",3333);
        Estudiante estudiante1 = new Estudiante("mateo","guzman", 19, 123, "capital",688);
        Estudiante estudiante2 = new Estudiante("martin","moreno", 20, 465, "chumbicha",28);
        curso1.agregarEstudiante(estudiante1);
        curso1.agregarEstudiante(estudiante2);
        assertTrue(curso1.existeEstudianteLlamado("guzman"));
    }
    @Test 
    public void noexisteEstudianteLlamado(){
        Curso curso1 =new Curso("python",3333);
        Estudiante estudiante1 = new Estudiante("mateo","guzman", 19, 123, "capital",688);
        Estudiante estudiante2 = new Estudiante("martin","moreno", 20, 465, "chumbicha",28);
        curso1.agregarEstudiante(estudiante1);
        curso1.agregarEstudiante(estudiante2);
        assertFalse(curso1.existeEstudianteLlamado("De la Fuente"));
    }
    @Test 
    public void buscarEstudiante(){
        Curso curso1 =new Curso("python",3333);
        Estudiante estudiante1 = new Estudiante("mateo","guzman", 19, 123, "capital",688);
        Estudiante estudiante2 = new Estudiante("martin","moreno", 20, 465, "chumbicha",28);
        curso1.agregarEstudiante(estudiante1);
        curso1.agregarEstudiante(estudiante2);
        assertEquals(estudiante1,curso1.buscarEstudiante(688));
    }
    @Test
    public void ingresarProfesor(){
        Curso curso1 =new Curso("calculo",3333);
        Profesor profesor1 = new Profesor("daniel", "lopez", 35, 515, "licenciado");
        Profesor profesor2 = new Profesor("Juan", "Villa", 29, 1665, "Ingeniero");
        curso1.agregarProfesor(profesor1);
        curso1.agregarProfesor(profesor2);
        assertEquals(2,curso1.cantidadDeProfesor());
    }
    public void buscarProfesor(){
        Curso curso1 =new Curso("python",3333);
        Profesor profesor1 = new Profesor("daniel", "lopez", 35, 515, "licenciado");
        Profesor profesor2 = new Profesor("Juan", "Villa", 29, 1665, "Ingeniero");
        curso1.agregarProfesor(profesor1);
        curso1.agregarProfesor(profesor2);
        assertEquals(profesor1,curso1.buscarProfesor("lopez", "daniel"));
    }



}
