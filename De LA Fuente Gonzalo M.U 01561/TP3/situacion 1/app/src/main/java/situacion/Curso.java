package situacion;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Curso {
    private String nombreCurso;
    private int Codigo;
    private ArrayList<Estudiante> listaDeEstudiantes = new ArrayList<Estudiante>();
    private ArrayList<Profesor> listaDeProfesor = new ArrayList<Profesor>();

    public Curso(String nombre,int codigo){
        this.nombreCurso=nombre;
        this.Codigo=codigo;
        
    }
    public String getNombreDelCurso(){
        return nombreCurso;
    }
    public void resetearNotas(){
        for(Estudiante estudiante : listaDeEstudiantes){
            estudiante.setCalificacion(0);
        }
    }
    public void agregarEstudiante(Estudiante estudiante){
        listaDeEstudiantes.add(estudiante);

    }
    public int cantidadDeEstudiantesInscriptos(){
        int contador=0;
        for(Estudiante aux:listaDeEstudiantes){
            contador++;
        }
        return contador;

    }
    public boolean existeEstudianteConNotaDiez(){
        boolean respuesta=false;

        for(Estudiante aux : listaDeEstudiantes){
            if(aux.getCalificacion()==10){
                return true;
            }
        }
        return respuesta;
          

    }
    public ArrayList<Estudiante> estudiantes(){
        return listaDeEstudiantes;
    }
    public ArrayList<Estudiante> estudiantesAprobados(){
        ArrayList<Estudiante> alumnosAprobados = new ArrayList<Estudiante>();
        for (Estudiante aux:listaDeEstudiantes){
            if(aux.getCalificacion()>=4){
                alumnosAprobados.add(aux);

            }
        }
        return alumnosAprobados;
    }
    public int cantidadDeAlumnosAprobados(){
        int contador=0;
        for(Estudiante aux:listaDeEstudiantes){
            if(aux.getCalificacion()>=4){
                contador++;
            }
        }
        return contador;
    }
    public double porcentajeDeAprobados(){
        double porcentajeDeAprobados;
        porcentajeDeAprobados= (cantidadDeAlumnosAprobados()*100)/cantidadDeEstudiantesInscriptos();
        return porcentajeDeAprobados;

    }
    public double promedioDeCalificaciones(){
        double acumulador =0;
        for(Estudiante aux:listaDeEstudiantes){
            acumulador= acumulador + aux.getCalificacion();
        }
        double promedio = acumulador / cantidadDeEstudiantesInscriptos();
        return promedio;

    }
    public ArrayList<String> ciudadesExceptoCapital(){

        ArrayList<String> departamentoDeLosEstudiantes = new ArrayList<>();
        ArrayList<String> deparamentoSinRepetir = new ArrayList<>();
        for(Estudiante aux: listaDeEstudiantes){
            if(aux.getDepartamento()!="capital"){
                departamentoDeLosEstudiantes.add(aux.getDepartamento());

            }
            
        }
        Set<String> hashSet = new HashSet<>(departamentoDeLosEstudiantes);
        deparamentoSinRepetir.addAll(hashSet);
        return deparamentoSinRepetir;



    }
    public boolean unDesastre(){
        for(Estudiante aux:listaDeEstudiantes){
            if(aux.getCalificacion()>=4){
                return false;
            }
        }
        return true;
    }
    public boolean existeEstudiante(int matricula){
        for(Estudiante aux:listaDeEstudiantes){
            if(aux.getMatricula()==matricula){
                return true;
            }
        }
        return false;

    }
    
    public boolean existeEstudianteLlamado(String apellido){
        for(Estudiante aux:listaDeEstudiantes){
            if(aux.getApellido()==apellido){
                return true;
            }
        }
        return false;

    }
    
    public ArrayList<Estudiante> estudianteDelInteriorProvincial(){
        ArrayList<Estudiante> alumnosDelInterior = new ArrayList<Estudiante>();
        for (Estudiante aux:listaDeEstudiantes){
            if(aux.getDepartamento()!="capital"){
                alumnosDelInterior.add(aux);

            }
        }
        return alumnosDelInterior;
    }
    public Estudiante buscarEstudiante(int matricula){
        Estudiante estudianteEncontrado=null;
        for(Estudiante aux:listaDeEstudiantes){
            if(aux.getMatricula()==matricula){
                estudianteEncontrado=aux;
                break;
                
            }
        }
        return estudianteEncontrado;
    }
    public void eliminarAlumnos(int matricula){
        Estudiante estudianteEncontrado= buscarEstudiante(matricula);
        listaDeProfesor.remove(estudianteEncontrado);
    }
    public void agregarProfesor(Profesor profesor ){
        listaDeProfesor.add(profesor);

    }
    public int cantidadDeProfesor(){
        int contador=0;
        for(Profesor aux:listaDeProfesor){
            contador++;
        }
        return contador;

    }
    public Profesor buscarProfesor(String apellido, String nombre){
        Profesor profesorEncontrado=null;
        for(Profesor aux:listaDeProfesor){
            if(aux.getApellido()==apellido && aux.getNombre()==nombre){
                profesorEncontrado=aux;
                break;
            }
        }
        return profesorEncontrado;
        


        
    }
    public void eliminarProfesor(String apellido, String nombre){
        Profesor profesorEncontrado= buscarProfesor(apellido,nombre);
        listaDeProfesor.remove(profesorEncontrado);
    }
    public ArrayList<Profesor> profesoresDelCurso(){
        return listaDeProfesor;
    }
    


}
