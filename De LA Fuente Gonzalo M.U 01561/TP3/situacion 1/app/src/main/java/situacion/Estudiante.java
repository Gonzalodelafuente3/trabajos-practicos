package situacion;

public class Estudiante extends Persona{
    
    private int matricula;
    private double calificacion;
    private String departamentoProvincial;

    public Estudiante(String nombre,String apellido,int edad,int dni,String departamento,int matricula){
 
        super(nombre,apellido,edad,dni);
        this.calificacion=0;
        this.departamentoProvincial=departamento;
        this.matricula=matricula;
    }
    public void setCalificacion(int nota){
        this.calificacion=nota;
    }
 
    public double getCalificacion(){
        return calificacion;
    }
    public int getMatricula(){
        return matricula;
    }

    public String getDepartamento(){
        return departamentoProvincial;
    }
    


}