package situacion;

public abstract class Persona {
    private String nombre;
    private String apellido;
    private int edad;
    private int dni;

    public Persona(String nombre,String apellido,int edad,int dni){
        this.nombre=nombre;
        this.apellido=apellido;
        this.edad=edad;
        this.dni=dni;
    }
    public void  setNombre(String nombre){
        this.nombre=nombre;
    }
    public String getApellido(){
        return apellido;
    }
    public String getNombre(){
        return nombre;
    }


    
}
