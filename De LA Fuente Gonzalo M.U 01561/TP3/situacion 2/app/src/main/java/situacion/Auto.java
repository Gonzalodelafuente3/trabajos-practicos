package situacion;

public class Auto extends VehiculoParaAlquiler {
    

    public Auto(String marca,String patente,double precio){
        super(marca, patente, precio);
    }
    @Override
    public double calcularPrecioDeAlquiler(int dia){
        double operacion = dia * getPrecioBaseDeAlquiler();
        return operacion;
    }
}
