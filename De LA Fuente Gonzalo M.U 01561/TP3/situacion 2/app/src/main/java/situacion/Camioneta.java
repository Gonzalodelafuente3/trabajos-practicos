package situacion;

public class Camioneta extends VehiculoParaAlquiler{
    private double kmDelViaje;

    public Camioneta(String marca, String patente,double precio,double km){
        super(marca, patente, precio);
        this.kmDelViaje = km;
    }
    
    public double calcularPrecioDeAlquiler(int dia){
        double operacion=0;
        if(kmDelViaje<50){
            operacion= dia * getPrecioBaseDeAlquiler();
        }else{
            operacion = dia *(kmDelViaje*20);
        }
        return operacion;
        
    }
}
