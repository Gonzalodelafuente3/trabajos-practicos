package situacion;

import java.util.ArrayList;

public class AgenciaDeAlquiler {
    private String nombreDeLaAgencia;
    private String direccion;
    private long telefono;

    private ArrayList<Auto> listaDeAutos = new ArrayList<Auto>();
    private ArrayList<Minibus> listaDeMinibus = new ArrayList<Minibus>();
    private ArrayList<Camioneta> listaDeCamioneta = new ArrayList<Camioneta>();
    private ArrayList<Camion> listaDeCamiones = new ArrayList<Camion>();

    public AgenciaDeAlquiler(String nombre,String direccion,long tel){
        this.nombreDeLaAgencia=nombre;
        this.direccion=direccion;
        this.telefono=tel;

    }
    public String getNombre(){
        return nombreDeLaAgencia;
    }
    public String getDireccion(){
        return direccion;
    }
    public long getTelefono(){
        return telefono;
    }
    public void agregarAuto(Auto auto){
        listaDeAutos.add(auto);
    }
    public int cantidadDeAutos(){
        int contador=0;
        for(Auto aux:listaDeAutos){
            contador++;
        }
        return contador;

    }
    public Auto buscarAuto(String patente){
        Auto autoEncontrado=null;
        for(Auto aux:listaDeAutos){
            if(aux.getPatente()==patente){
                autoEncontrado=aux;
                break;
            }
        }
        return autoEncontrado;
        


        
    }
    public void eliminarAuto(String patente){
        Auto autoEncontrado= buscarAuto(patente);
        listaDeAutos.remove(autoEncontrado);
    }
    public ArrayList<Auto> listaDeAutos(){
        return listaDeAutos;
    }

    
    public void agregarMinibus(Minibus minibus){
        listaDeMinibus.add(minibus);
    }
    public int cantidadDeMinibus(){
        int contador=0;
        for(Minibus aux:listaDeMinibus){
            contador++;
        }
        return contador;

    }
    public Minibus buscarMinibus(String patente){
        Minibus minibusEncontrado=null;
        for(Minibus aux:listaDeMinibus){
            if(aux.getPatente()==patente){
                minibusEncontrado=aux;
                break;
            }
        }
        return minibusEncontrado;
        


        
    }
    public void eliminarMinibus(String patente){
        Minibus minibusEncontrado= buscarMinibus(patente);
        listaDeMinibus.remove(minibusEncontrado);
    }
    public ArrayList<Minibus> listaDeMinibus(){
        return listaDeMinibus;
    }

    public void agregarCamioneta(Camioneta camioneta){
        listaDeCamioneta.add(camioneta);
    }
    public int cantidadDeCamioneta(){
        int contador=0;
        for(Camioneta aux:listaDeCamioneta){
            contador++;
        }
        return contador;

    }
    public Camioneta buscarCamioneta(String patente){
        Camioneta camionetaEncontrada=null;
        for(Camioneta aux:listaDeCamioneta){
            if(aux.getPatente()==patente){
                camionetaEncontrada=aux;
                break;
            }
        }
        return camionetaEncontrada;
        


        
    }
    public void eliminarCamioneta(String patente){
        Camioneta camionetaEncontrad= buscarCamioneta(patente);
        listaDeCamioneta.remove(camionetaEncontrad);
    }
    public ArrayList<Camioneta> listaDeCamionetas(){
        return listaDeCamioneta;
    }


    public void agregarCamion(Camion camion){
        listaDeCamiones.add(camion);
    }
    public int cantidadDeCamion(){
        int contador=0;
        for(Camion aux:listaDeCamiones){
            contador++;
        }
        return contador;

    }
    public Camion buscarCamion(String patente){
        Camion camionEncontrado=null;
        for(Camion aux:listaDeCamiones){
            if(aux.getPatente()==patente){
                camionEncontrado=aux;
                break;
            }
        }
        return camionEncontrado;
        


        
    }
    public void eliminarCamion(String patente){
        Camion camionEncontrado= buscarCamion(patente);
        listaDeCamiones.remove(camionEncontrado);
    }
    public ArrayList<Camion> listaDeCamiones(){
        return listaDeCamiones;
    }


}
