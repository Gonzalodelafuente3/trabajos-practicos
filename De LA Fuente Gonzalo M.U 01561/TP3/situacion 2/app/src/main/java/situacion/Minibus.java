package situacion;

public class Minibus extends VehiculoParaAlquiler{
    private double seguroPorPlaza;

    public Minibus(String marca,String patente, double precio, double seguro){
        super(marca, patente, precio);
        this.seguroPorPlaza=seguro;
    }
    @Override
    public double calcularPrecioDeAlquiler(int dia){
        double operacion = (dia * getPrecioBaseDeAlquiler())+seguroPorPlaza;
        return operacion;
    }

    


}
