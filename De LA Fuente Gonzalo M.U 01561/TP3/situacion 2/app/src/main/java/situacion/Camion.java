package situacion;

public class Camion extends VehiculoParaAlquiler {
    private double kmDelViaje;
    private double abonoExtra;

    public Camion(String marca, String patente,double precio,double km, double abonoExtra){
        super(marca, patente, precio);
        this.kmDelViaje = km;
        this.abonoExtra=abonoExtra;
    }
    
    public double calcularPrecioDeAlquiler(int dia){
        double operacion=0;
        if(kmDelViaje<50){
            operacion= dia * getPrecioBaseDeAlquiler();
        }else{
            operacion = dia *(kmDelViaje*20);
        }
        return operacion + abonoExtra;
        
    }
}
