package situacion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CamionTest {
    @Test
    public void alquilerDeUnCamionMenorA50Km(){
        Camion camion = new Camion("Iveco", "GRA333", 300, 49, 200);
        int diaDeAlquiler=45;

        assertEquals(13700,camion.calcularPrecioDeAlquiler(diaDeAlquiler),0);
    }
    @Test
    public void alquilerDeCamionMayorA50Km(){
        Camion camion = new Camion("Iveco", "GRA333", 300, 51, 200);
        int diaDeAlquiler=110;

        assertEquals(112400,camion.calcularPrecioDeAlquiler(diaDeAlquiler),0);
    }
}
