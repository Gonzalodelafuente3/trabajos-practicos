package situacion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MinibusTest {
    @Test
    public void AlquilerDeMinubus(){
        Minibus alquilermMinibus = new Minibus("ford","AFT015",50,250);
        int diaQueVoyAAlquilarElVehiiculo=8;

        assertEquals(650,alquilermMinibus.calcularPrecioDeAlquiler(diaQueVoyAAlquilarElVehiiculo),0);
    }
}
