package situacion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AgenciaDeAlquilerTest {
    @Test
    public void ingresarUnAuto(){
        AgenciaDeAlquiler agencia1 = new AgenciaDeAlquiler("Alquiler don diego", "zona centro", 14523697);
        Auto auto1 = new Auto("Ford", "ALT039", 50);

        agencia1.agregarAuto(auto1);
        assertEquals(1,agencia1.cantidadDeAutos());
    }
    @Test
    public void ingresarMasDeUnAuto(){
        AgenciaDeAlquiler agencia1 = new AgenciaDeAlquiler("Alquiler don diego", "zona centro", 14523697);
        Auto auto1 = new Auto("Ford", "ALT039", 50);
        Auto auto2 = new Auto("Toyota", "ALO369", 50);
        Auto auto3 = new Auto("Ford", "PRT464", 50);
        agencia1.agregarAuto(auto1);
        agencia1.agregarAuto(auto2);
        agencia1.agregarAuto(auto3);
        assertEquals(3,agencia1.cantidadDeAutos());
    }
    @Test 
    public void buscarAuto(){
        AgenciaDeAlquiler agencia1 = new AgenciaDeAlquiler("Alquiler don diego", "zona centro", 14523697);
        Auto auto1 = new Auto("Ford", "ALT039", 50);
        Auto auto2 = new Auto("Toyota", "ALO369", 50);
        
        agencia1.agregarAuto(auto1);
        agencia1.agregarAuto(auto2);
        assertEquals(auto1,agencia1.buscarAuto("ALT039"));
    }


    @Test
    public void ingresarUnaCamioneta(){
        AgenciaDeAlquiler agencia1 = new AgenciaDeAlquiler("Alquiler don diego", "zona centro", 14523697);
        Camioneta camioneta = new Camioneta("Ford", "AAT039", 300,30);

        agencia1.agregarCamioneta(camioneta);
        assertEquals(1,agencia1.cantidadDeCamioneta());
    }
    @Test
    public void ingresarMasDeUnaCamioneta(){
        AgenciaDeAlquiler agencia1 = new AgenciaDeAlquiler("Alquiler don diego", "zona centro", 14523697);
        Camioneta camioneta1 = new Camioneta("Ford", "AAT039", 300,30);
        Camioneta camioneta2 = new Camioneta("Toyota", "LOI039", 300,70);
        Camioneta camioneta3 = new Camioneta("Fiat", "NMJ039", 300,80);


        agencia1.agregarCamioneta(camioneta1);
        agencia1.agregarCamioneta(camioneta2);
        agencia1.agregarCamioneta(camioneta3);
        assertEquals(3,agencia1.cantidadDeCamioneta());
    }
    @Test 
    public void buscarunaCamioneta(){
        AgenciaDeAlquiler agencia1 = new AgenciaDeAlquiler("Alquiler don diego", "zona centro", 14523697);
        
        Camioneta camioneta2 = new Camioneta("Toyota", "LOI039", 300,70);
        Camioneta camioneta = new Camioneta("Fiat", "NMJ039", 300,80);


        
        agencia1.agregarCamioneta(camioneta2);
        agencia1.agregarCamioneta(camioneta);
        assertEquals(camioneta,agencia1.buscarCamioneta("NMJ039"));
    }
    @Test
    public void ingresarUnCamion(){
        AgenciaDeAlquiler agencia1 = new AgenciaDeAlquiler("Alquiler don diego", "zona centro", 14523697);
        Camion camion = new Camion("Ford", "AAT816", 300,30,200);


        agencia1.agregarCamion(camion);
        assertEquals(1,agencia1.cantidadDeCamion());
    }
    @Test
    public void ingresarMasDeUnCamion(){
        AgenciaDeAlquiler agencia1 = new AgenciaDeAlquiler("Alquiler don diego", "zona centro", 14523697);
        Camion camion = new Camion("Ford", "AAT816", 300,30,200);
        Camion camion1 = new Camion("Iveco", "GRA333", 300, 51, 200);
        Camion camion2 = new Camion("Iveco", "GFG313", 300, 49, 200);


        agencia1.agregarCamion(camion);
        agencia1.agregarCamion(camion1);
        agencia1.agregarCamion(camion2);
        assertEquals(3,agencia1.cantidadDeCamion());
    }
    public void buscarUnCamion(){
        AgenciaDeAlquiler agencia1 = new AgenciaDeAlquiler("Alquiler don diego", "zona centro", 14523697);
        Camion camion = new Camion("Ford", "AAT816", 300,30,200);
        Camion camion1 = new Camion("Iveco", "GRA333", 300, 51, 200);
        Camion camion2 = new Camion("Iveco", "GFG313", 300, 49, 200);


        agencia1.agregarCamion(camion);
        agencia1.agregarCamion(camion1);
        agencia1.agregarCamion(camion2);
        assertEquals(camion1,agencia1.buscarCamion("GFG313"));
    }
    @Test
    public void ingresarUnMinibus(){
        AgenciaDeAlquiler agencia1 = new AgenciaDeAlquiler("Alquiler don diego", "zona centro", 14523697);
        Minibus minibus = new Minibus("ford","AFT015",50,250);
        agencia1.agregarMinibus(minibus);
        assertEquals(1,agencia1.cantidadDeMinibus());
    }
    @Test
    public void ingresarMasDeUnMinibus(){
        AgenciaDeAlquiler agencia1 = new AgenciaDeAlquiler("Alquiler don diego", "zona centro", 14523697);
        Minibus minibus = new Minibus("ford","AFT015",50,250);
        Minibus minibus1 = new Minibus("Toyota","AFT684",50,250);
        Minibus minibus2 = new Minibus("ford","GLO015",50,250);
        Minibus minibus3 = new Minibus("Fiat","POI015",50,250);
        
        agencia1.agregarMinibus(minibus);
        agencia1.agregarMinibus(minibus1);
        agencia1.agregarMinibus(minibus2);
        agencia1.agregarMinibus(minibus3);
        assertEquals(4,agencia1.cantidadDeMinibus());
    }
    @Test    
    public void buscarMinibus(){
        AgenciaDeAlquiler agencia1 = new AgenciaDeAlquiler("Alquiler don diego", "zona centro", 14523697);
        
        Minibus minibus = new Minibus("ford","AFT015",50,250);
        Minibus minibus1 = new Minibus("Toyota","AFT684",50,250);
        Minibus minibus2 = new Minibus("ford","GLO015",50,250);
        agencia1.agregarMinibus(minibus);
        agencia1.agregarMinibus(minibus1);
        agencia1.agregarMinibus(minibus2);

        assertEquals(minibus,agencia1.buscarMinibus("AFT015"));
    }


}
