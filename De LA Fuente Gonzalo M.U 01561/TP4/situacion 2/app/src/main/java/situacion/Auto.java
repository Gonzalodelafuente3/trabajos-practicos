package situacion;

import java.util.ArrayList;
import java.util.List;

public abstract class  Auto extends VehiculoPasajero implements PrecioDeVenta{
    private double precioBaseVenta;
    private List<ComponentesExtras> listaDeComponentes = new ArrayList<ComponentesExtras>();
    public Auto(double precioPlazaYDia, String marca, String patente, double precioBaseVenta) {
        super(precioPlazaYDia, marca, patente);
        this.precioBaseVenta= precioBaseVenta;
    }
    public List<ComponentesExtras> listaDeComponentes(){
        return listaDeComponentes;
    }
    public void agregarComponente(ComponentesExtras componente){
        listaDeComponentes.add(componente);
    }
    public double getPrecioBaseVenta(){
        return precioBaseVenta;
    }
    public void setPrecioBaseVenta(double precio){
        this.precioBaseVenta=precio;
    }

    public abstract double calcularPrecioDeVenta();

    
    
}
