package situacion;

public class Minibus extends VehiculoPasajero implements PrecioDeAlquiler{
    private double seguroPorPlaza;
    private double precioBaseAlquiler;

    public Minibus(double seguroPorPlaza, double precioPlazaYDia, String marca, String patente,double precioBaseAlquiler) {
        super(precioPlazaYDia, marca, patente);
        this.seguroPorPlaza = seguroPorPlaza;
        this.precioBaseAlquiler= precioBaseAlquiler;
    }

    public double getPrecioBaseAlquiler(){
        return precioBaseAlquiler;
    }
    @Override
    public double calcularPrecioDeAlquiler(int dia){
        double operacion = precioBaseAlquiler + (getPrecioPlazaYDia() *dia) + seguroPorPlaza ;
        
        return operacion;
    }


    

    


}
