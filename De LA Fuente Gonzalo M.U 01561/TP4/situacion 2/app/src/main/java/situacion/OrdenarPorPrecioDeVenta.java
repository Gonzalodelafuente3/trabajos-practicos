package situacion;

import java.util.Comparator;

public class OrdenarPorPrecioDeVenta implements Comparator{

    @Override
    public int compare(Object o1, Object o2) {
        Auto v1= (Auto)o1;
        Auto v2= (Auto)o2;
        int respuesta = -1;
        if(v1.getPrecioBaseVenta()==v2.getPrecioBaseVenta()){
            respuesta = (int) (v1.getPrecioBaseVenta()-v2.getPrecioBaseVenta());
        }else{
            respuesta = (int) (v1.getPrecioBaseVenta() - v2.getPrecioBaseVenta());
        }

        return respuesta;
        
    }

}
