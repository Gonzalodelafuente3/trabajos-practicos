/*Se debe programar una aplicación para una agencia de alquiler de autos. Se necesita poder determinar el 
precio de alquiler de los vehículos con que cuenta la empresa que alquila vehículos tanto de pasa- jeros
 como de carga. De los vehículos se almacena marca, patente, el precio base de alquiler. Los vehículos de 
 alquiler pueden ser autos, minibús, camionetas y camiones. Los vehículos se deben re- gistrar al ingresar 
 al parque automotor para estar disponibles para alquilar o vender. El precio de alquiler se compone de un
  precio base por día. En el caso de coches se suma de $50 por y plaza por día. El precio de alquiler del
   microbús es igual al de los coches más $250 de seguro por plaza. El precio de alquiler de los vehículos
   de carga es de $300 si el viaje es menor a 50 Km. En caso contrario su precio será de $20 multiplicado 
   por kilómetro recorrido. Para los camiones se debe abonar $200 extras independientemente del kilometraje 
   recorrido. Se debe poder mostrar el precio obtenido para cada caso.*/
package situacion;




public abstract class Vehiculo {
    private String marca;
    private String patente;
    

    public Vehiculo(String marca, String patente) {
        this.marca = marca;
        this.patente = patente;
        
    }

    public String getMarca() {
        return marca;
    }

    
    
    public String getPatente(){
        return patente;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    
    
    
    

}


