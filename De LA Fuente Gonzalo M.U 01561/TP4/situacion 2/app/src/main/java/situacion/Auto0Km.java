package situacion;

public class Auto0Km extends Auto{

    public Auto0Km(double precioPlazaYDia, String marca, String patente, double precioBaseVenta) {
        super(precioPlazaYDia, marca, patente, precioBaseVenta);
        //TODO Auto-generated constructor stub
    }

    @Override
    public double calcularPrecioDeVenta() {
        double precio=0;
        for(ComponentesExtras aux : listaDeComponentes()){
            precio =  precio + (getPrecioBaseVenta() * aux.getPorcentaje())/100;
        }
        precio = precio + getPrecioBaseVenta();
        return precio;
    }
    
}
