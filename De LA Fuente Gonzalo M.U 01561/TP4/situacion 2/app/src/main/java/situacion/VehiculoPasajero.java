package situacion;

import java.util.ArrayList;
import java.util.List;

public abstract class VehiculoPasajero extends Vehiculo {
    private double precioPlazaYDia;
    
    

    public VehiculoPasajero(double precioPlazaYDia, String marca, String patente) {
        super(marca, patente);
        this.precioPlazaYDia = precioPlazaYDia;
        
    }
    

    public double getPrecioPlazaYDia() {
        return precioPlazaYDia;
    }

    public void setPrecioPlazaYDia(double precioPlazaYDia) {
        this.precioPlazaYDia = precioPlazaYDia;
    }

    
    
   
    
}
