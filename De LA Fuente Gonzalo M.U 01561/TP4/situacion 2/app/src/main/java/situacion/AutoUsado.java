package situacion;

public class AutoUsado extends Auto implements PrecioDeAlquiler{
    private double precioBaseAlquiler;
    public AutoUsado(double precioPlazaYDia, String marca, String patente, double precioBaseVenta,double precioBaseAlquiler) {
        super(precioPlazaYDia, marca, patente, precioBaseVenta);
        this.precioBaseAlquiler=precioBaseAlquiler;
        
    }
    public double getPrecioBaseAlquiler(){
        return precioBaseAlquiler;
    }

    @Override
    public double calcularPrecioDeVenta() {
        
        return getPrecioBaseVenta();
    }

    @Override
    public double calcularPrecioDeAlquiler(int dia){
        double operacion = precioBaseAlquiler + (getPrecioPlazaYDia() *dia) ;
        return operacion;
    }
    
}
