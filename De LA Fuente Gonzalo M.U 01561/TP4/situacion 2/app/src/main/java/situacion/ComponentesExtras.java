/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package situacion;

/**
 *
 * @author Usuario
 */
public class ComponentesExtras {
    private String nombreComponente;
    private double porcentaje;

    public ComponentesExtras(String nombreComponente, double porcentaje) {
        this.nombreComponente = nombreComponente;
        this.porcentaje = porcentaje;
    }

    public String getNombreComponente() {
        return nombreComponente;
    }

    public double getPorcentaje() {
        return porcentaje;
    }

    public void setNombreComponente(String nombreComponente) {
        this.nombreComponente = nombreComponente;
    }

    public void setPorcentaje(double porcentaje) {
        this.porcentaje = porcentaje;
    }

    
}
