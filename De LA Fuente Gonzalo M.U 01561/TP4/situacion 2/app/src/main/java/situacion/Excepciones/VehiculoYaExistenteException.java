package situacion.Excepciones;

public class VehiculoYaExistenteException extends RuntimeException{
    public VehiculoYaExistenteException(String mensaje){
        super(mensaje);
    }
}
