package situacion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import situacion.Excepciones.VehiculoNoEncontradoException;
import situacion.Excepciones.VehiculoYaExistenteException;
import situacion.Excepciones.VehiculosNoExistenteException;

public class Agencia {
    private String nombreDeLaAgencia;
    private String direccion;
    private long telefono;

    private List<Vehiculo> listaDeVehiculos = new ArrayList<Vehiculo>(); 

    public Agencia(String nombre,String direccion,long tel){
        this.nombreDeLaAgencia=nombre;
        this.direccion=direccion;
        this.telefono=tel;

    }
    public String getNombre(){
        return nombreDeLaAgencia;
    }
    public String getDireccion(){
        return direccion;
    }
    public long getTelefono(){
        return telefono;
    }

    public void setNombreDeLaAgencia(String nombreDeLaAgencia) {
        this.nombreDeLaAgencia = nombreDeLaAgencia;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }
    
    public List<Vehiculo> getlistaDeVehiculos(){
        return listaDeVehiculos;
    }
    public void agregarVehiculo(Vehiculo vehiculo){
        for(Vehiculo aux: listaDeVehiculos){
            if(aux.getPatente().equals(vehiculo.getPatente())){
                throw new VehiculoYaExistenteException("El Vehiculo Ya esta en la Lista");
            }
        }
        listaDeVehiculos.add(vehiculo);

    }
    public int cantidadDeVehiculos(){
        int contador =0;
        for(Vehiculo aux:listaDeVehiculos){
            contador++;
        }
        return contador;
    }
    public Vehiculo buscarVehiculo(String patente){
        Vehiculo encontrado = null;
        for(Vehiculo aux: listaDeVehiculos){
            if(aux.getPatente().equals(patente)){
                encontrado=aux;
                break;
            }
        }
        if(encontrado == null){
            throw new VehiculoNoEncontradoException("El Vehiculo no se puedo Encontrar");
        }
        return encontrado;
    }
    public void eliminarVehiculo(String patente ){
        Vehiculo encontrado=buscarVehiculo(patente);
        listaDeVehiculos.remove(encontrado);

    }
    
    public List<Auto> obtenerListaDeVehiculosEnVenta(){
        List<Auto> listaDeAuto = new ArrayList<Auto>();
        for(Object object : listaDeVehiculos){
            if(object instanceof Auto  ){
                listaDeAuto.add((Auto)object);
            }
        }
        
        return listaDeAuto;
    }
    public int cantidadDeAutos(){
        int contador=0;
        for(Vehiculo aux : listaDeVehiculos){
            if(aux instanceof Auto){
                contador ++;
            }
        }
        return contador;
    }
    
    public int cantidadDeMinibus(){
        int contador=0;
        for(Vehiculo aux : listaDeVehiculos){
            if(aux instanceof Minibus){
                contador ++;
            }
        }
        return contador;
    }
    
     
    public int cantidadDeCamionetas(){
        int contador=0;
        for(Vehiculo aux : listaDeVehiculos){
            if(aux instanceof Camioneta){
                contador ++;
            }
        }
        return contador;
    }
    
       

    public int cantidadDeCamion(){
        int contador=0;
        for(Vehiculo aux : listaDeVehiculos){
            if(aux instanceof Camion){
                contador ++;
            }
        }
        return contador;
    }
    public List<Vehiculo> obtenerListaDeVehiculosEnAlquiler(){
        List<Vehiculo> listaDeVehiculoEnVenta = new ArrayList<Vehiculo>();
        for(Object object : listaDeVehiculos){
            if(object instanceof VehiculoDeCarga || object instanceof Minibus || object instanceof AutoUsado ){
                listaDeVehiculoEnVenta.add((Vehiculo)object);
            }
        }
        
        return listaDeVehiculoEnVenta;
    }
    public List<Auto> ordenarPorPrecioDeVenta(){
        List<Auto> listaOrdenada = obtenerListaDeVehiculosEnVenta();
        Collections.sort(listaOrdenada ,new OrdenarPorPrecioDeVenta());
        return listaOrdenada;

    }


}
