package situacion;

public abstract class VehiculoDeCarga extends Vehiculo implements PrecioDeAlquiler{
    private double kmRecorrido;
    private double precioBaseAlquiler;

    public VehiculoDeCarga(double kmRecorrido, String marca, String patente, double precioBaseAlquiler) {
        super(marca, patente);
        this.kmRecorrido = kmRecorrido;
        this.precioBaseAlquiler=precioBaseAlquiler;
    }

    

    public double getKmRecorrido() {
        return kmRecorrido;
    }

    public void setKmRecorrido(double kmRecorrido) {
        this.kmRecorrido = kmRecorrido;
    }

    public double getPrecioBaseAlquiler() {
        return precioBaseAlquiler;
    }

    public void setPrecioBaseAlquiler(double precioBaseAlquiler) {
        this.precioBaseAlquiler = precioBaseAlquiler;
    }

    
    public abstract double calcularPrecioDeAlquiler(int dia);
}
