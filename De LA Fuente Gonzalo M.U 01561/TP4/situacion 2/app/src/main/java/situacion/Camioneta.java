package situacion;

public class Camioneta extends VehiculoDeCarga{
    

    public Camioneta(double kmRecorrido, String marca, String patente, double precioBaseAlquiler) {
        super(kmRecorrido, marca, patente, precioBaseAlquiler);
        
    }

    
    @Override
    public double calcularPrecioDeAlquiler(int dia){
        double operacion=0;
        if(getKmRecorrido()<=50){
            operacion= getPrecioBaseAlquiler() *dia;
        }else{
            operacion = (20 * getKmRecorrido())*dia;
        }
        return operacion;
        
    }
}
