package situacion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CamionTest {
    @Test
    public void alquilerDeUnCamionMenorA50Km(){
        Camion camion = new Camion(200, 50, "Toyota", "LKJ369", 300);
        int diaDeAlquiler=45;

        assertEquals(13700,camion.calcularPrecioDeAlquiler(diaDeAlquiler),0);
    }
    @Test
    public void alquilerDeCamionMayorA50Km(){
        Camion camion = new Camion(200, 60, "Iveco", "JLO789", 300);
        int diaDeAlquiler=110;

        assertEquals(132200,camion.calcularPrecioDeAlquiler(diaDeAlquiler),0);
    }
}
