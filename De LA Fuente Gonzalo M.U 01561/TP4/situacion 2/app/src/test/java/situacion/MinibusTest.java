package situacion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MinibusTest {
    @Test
    public void AlquilerDeMinubus(){
        Minibus alquilerMinibus = new Minibus(250, 50, "ford", "HOL123", 2000);
        int diaQueVoyAAlquilarElVehiiculo=8;

        assertEquals(2650,alquilerMinibus.calcularPrecioDeAlquiler(diaQueVoyAAlquilarElVehiiculo),0);
    }
}
