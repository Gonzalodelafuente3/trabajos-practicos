package situacion;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.lang.reflect.Array;

import org.junit.Test;

import situacion.Excepciones.VehiculoNoEncontradoException;
import situacion.Excepciones.VehiculoYaExistenteException;

public class AgenciaDeAlquilerTest {
    @Test
    public void ingresarVehiculos(){
        Agencia agencia1 = new Agencia("Alquiler don diego", "zona centro", 14523697);
        Auto auto1 = new Auto0Km(50, "ford", "FLO456", 8000);
        Camion camion1 = new Camion(200, 50, "iveco", "QWE568", 300);
        Camioneta camioneta1 = new Camioneta(100, "ford", "ERT789", 300);
        Minibus minibus1 = new Minibus(250, 50, "Toyota", "XCV369", 5000);
        agencia1.agregarVehiculo(auto1);
        agencia1.agregarVehiculo(camion1);
        agencia1.agregarVehiculo(camioneta1);
        agencia1.agregarVehiculo(minibus1);
        assertEquals(4,agencia1.cantidadDeVehiculos());
    }
   

    @Test 
    public void buscarVehiculo(){
        Agencia agencia1 = new Agencia("Alquiler don diego", "zona centro", 14523697);
        Auto auto1 = new Auto0Km(50, "ford", "FLO456", 8000);
        Camion camion1 = new Camion(200, 50, "iveco", "QWE568", 300);
        Camioneta camioneta1 = new Camioneta(100, "ford", "ERT789", 300);
        Minibus minibus1 = new Minibus(250, 50, "Toyota", "XCV369", 5000);
        agencia1.agregarVehiculo(auto1);
        agencia1.agregarVehiculo(camion1);
        agencia1.agregarVehiculo(camioneta1);
        agencia1.agregarVehiculo(minibus1);
        assertEquals(auto1,agencia1.buscarVehiculo("FLO456"));
    }
    @Test
    public void eliminarVehiculo(){
        Agencia agencia1 = new Agencia("Alquiler don diego", "zona centro", 14523697);
        Auto auto1 = new Auto0Km(50, "ford", "FLO456", 8000);
        Camion camion1 = new Camion(200, 50, "iveco", "QWE568", 300);
        Camioneta camioneta1 = new Camioneta(100, "ford", "ERT789", 300);
        Minibus minibus1 = new Minibus(250, 50, "Toyota", "XCV369", 5000);
        agencia1.agregarVehiculo(auto1);
        agencia1.agregarVehiculo(camion1);
        agencia1.agregarVehiculo(camioneta1);
        agencia1.agregarVehiculo(minibus1);
        agencia1.eliminarVehiculo("QWE568");
        assertEquals(3,agencia1.cantidadDeVehiculos());
    }
    @Test
    public void cantidadDeAutos(){
        Agencia agencia1 = new Agencia("Alquiler don diego", "zona centro", 14523697);
        Auto auto1 = new Auto0Km(50, "ford", "FLO456", 8000);
        Camion camion1 = new Camion(200, 50, "iveco", "QWE568", 300);
        Camioneta camioneta1 = new Camioneta(100, "ford", "ERT789", 300);
        Minibus minibus1 = new Minibus(250, 50, "Toyota", "XCV369", 5000);
        Auto auto2 = new Auto0Km(50, "fiat", "MNB789", 6000);
        agencia1.agregarVehiculo(auto1);
        agencia1.agregarVehiculo(camion1);
        agencia1.agregarVehiculo(camioneta1);
        agencia1.agregarVehiculo(minibus1);
        agencia1.agregarVehiculo(auto2);
        assertEquals(2, agencia1.cantidadDeAutos());

    }
    @Test
    public void cantidadDeMinibus(){
        Agencia agencia1 = new Agencia("Alquiler don diego", "zona centro", 14523697);
        Auto auto1 = new Auto0Km(50, "ford", "FLO456", 8000);
        Minibus minibus2 = new Minibus(250, 50, "ford", "FOR564", 7000);
        Minibus minibus3 = new Minibus(250, 50, "ford", "POI456", 9000);
        Minibus minibus1 = new Minibus(250, 50, "Toyota", "XCV369", 5000);
        agencia1.agregarVehiculo(auto1);
       
        agencia1.agregarVehiculo(minibus1);
        agencia1.agregarVehiculo(minibus2);
        agencia1.agregarVehiculo(minibus3);
        assertEquals(3, agencia1.cantidadDeMinibus());
    }
    @Test
    public void cantidadDeCamiones(){
        Agencia agencia1 = new Agencia("Alquiler don diego", "zona centro", 14523697);
        Auto auto1 = new Auto0Km(50, "ford", "FLO456", 8000);
        Camion camion1 = new Camion(200, 50, "iveco", "QWE568", 300);
        Camion camion2 = new Camion(200, 50, "iveco", "WEP698", 300);
        agencia1.agregarVehiculo(auto1);
        agencia1.agregarVehiculo(camion1);
        agencia1.agregarVehiculo(camion2);
        
        assertEquals(2, agencia1.cantidadDeCamion());
    }
    @Test
    public void cantidadDeCamionetas(){
        Agencia agencia1 = new Agencia("Alquiler don diego", "zona centro", 14523697);
        Auto auto1 = new Auto0Km(50, "ford", "FLO456", 8000);
        Camion camion1 = new Camion(200, 50, "iveco", "QWE568", 300);
        Camioneta camioneta1 = new Camioneta(100, "ford", "ERT789", 300);
        Minibus minibus1 = new Minibus(250, 50, "Toyota", "XCV369", 5000);
        agencia1.agregarVehiculo(auto1);
        agencia1.agregarVehiculo(camion1);
        agencia1.agregarVehiculo(camioneta1);
        agencia1.agregarVehiculo(minibus1);
        assertEquals(1, agencia1.cantidadDeCamionetas());
    }
    @Test(expected = VehiculoYaExistenteException.class)
    public void vehiculoYaExistente(){
        Agencia agencia1 = new Agencia("Alquiler don diego", "zona centro", 14523697);
        Auto auto1 = new Auto0Km(50, "ford", "FLO456", 8000);
        Camion camion1 = new Camion(200, 50, "iveco", "QWE568", 300);
        Camioneta camioneta1 = new Camioneta(100, "ford", "ERT789", 300);
        Minibus minibus1 = new Minibus(250, 50, "Toyota", "XCV369", 5000);
        Auto auto2 = new Auto0Km(50, "ford", "FLO456", 8000);
        agencia1.agregarVehiculo(auto1);
        agencia1.agregarVehiculo(camion1);
        agencia1.agregarVehiculo(camioneta1);
        agencia1.agregarVehiculo(minibus1);
        agencia1.agregarVehiculo(auto2);
    }
    @Test(expected = VehiculoNoEncontradoException.class)
    public void vehiculoNoEncontrado(){
        Agencia agencia1 = new Agencia("Alquiler don diego", "zona centro", 14523697);
        Auto auto1 = new Auto0Km(50, "ford", "FLO456", 8000);
        Camion camion1 = new Camion(200, 50, "iveco", "QWE568", 300);
        
        agencia1.agregarVehiculo(auto1);
        agencia1.agregarVehiculo(camion1);
        agencia1.buscarVehiculo("DLO259");
    }
    



   
    


}
