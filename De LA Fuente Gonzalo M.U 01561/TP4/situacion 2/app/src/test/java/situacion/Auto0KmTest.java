package situacion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Auto0KmTest {
    @Test
    public void venderAuto0kmConAire(){
        Auto0Km auto0km = new Auto0Km(50, "ford", "WER123", 800000);
        ComponentesExtras utilidad = new ComponentesExtras("utilidad", 50) ;
        ComponentesExtras componente = new ComponentesExtras("aire acondicionado", 2) ;
        auto0km.agregarComponente(utilidad);
        auto0km.agregarComponente(componente);
        assertEquals(1216000,auto0km.calcularPrecioDeVenta(),0);
    }
    @Test
    public void venderAuto0kmConLevantaCristales(){
        Auto0Km auto0km = new Auto0Km(50, "ford", "WER123", 800000);
        ComponentesExtras utilidad = new ComponentesExtras("utilidad", 50) ;
        ComponentesExtras componente = new ComponentesExtras("levanta cristales", 5) ;
        auto0km.agregarComponente(utilidad);
        auto0km.agregarComponente(componente);
        assertEquals(1240000,auto0km.calcularPrecioDeVenta(),0);
    }
    @Test
    public void venderAuto0kmConAlarma(){
        Auto0Km auto0km = new Auto0Km(50, "ford", "WER123", 800000);
        ComponentesExtras utilidad = new ComponentesExtras("utilidad", 50) ;
        ComponentesExtras componente = new ComponentesExtras("alarma", 1) ;
        auto0km.agregarComponente(utilidad);
        auto0km.agregarComponente(componente);
        assertEquals(1208000,auto0km.calcularPrecioDeVenta(),0);
    }
    @Test
    public void venderAuto0kmConTodo(){
        Auto0Km auto0km = new Auto0Km(50, "ford", "WER123", 800000);
        ComponentesExtras utilidad = new ComponentesExtras("utilidad", 50) ;
        ComponentesExtras componente = new ComponentesExtras("alarma", 1) ;
        ComponentesExtras componente2 = new ComponentesExtras("levanta cristales", 5) ;
        ComponentesExtras componente3 = new ComponentesExtras("aire acondicionado", 2) ;
        auto0km.agregarComponente(utilidad);
        auto0km.agregarComponente(componente);
        auto0km.agregarComponente(componente2);
        auto0km.agregarComponente(componente3);
        assertEquals(1264000,auto0km.calcularPrecioDeVenta(),0);
    }
}
