package situacion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CamionetaTest {
    @Test
    public void alquilerDeCamionetaMenorA50km(){
        Camioneta camioneta1 = new Camioneta(50, "Ford", "ADP654", 300);
        int diaDeAlquiler= 90;
        assertEquals(27000,camioneta1.calcularPrecioDeAlquiler(diaDeAlquiler),0);

    }
    @Test
    public void alquilerDeCamionetaMayorA50Km(){
        Camioneta camionetaUno = new Camioneta(80, "Ram", "VHU365", 300);
        int diaDeAlquiler= 300;
        assertEquals(480000,camionetaUno.calcularPrecioDeAlquiler(diaDeAlquiler),0);

    }
}
