package situacion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PersonaTest {
    @Test
    public void coincideDniTest(){
        Estudiante estudiante = new Estudiante("juan", "melon", 39, 3698, "capital", 6986,8);
        boolean respuesta = estudiante.coincideDni(3698);
        assertEquals(true,respuesta);
        
    }
    @Test
    public void coincideDniProfesor(){
        Profesor profesor = new Profesor("tito", "peña", 32, 123698, "Licenciado");
        boolean respuesta = profesor.coincideDni(123698);
        assertEquals(true,respuesta);
    }
    @Test
    public void coincideDniProfesorFalse(){
        Profesor profesor = new Profesor("tito", "peña", 32, 123698, "Licenciado");
        boolean respuesta = profesor.coincideDni(123);
        assertFalse(respuesta);
    }
}
