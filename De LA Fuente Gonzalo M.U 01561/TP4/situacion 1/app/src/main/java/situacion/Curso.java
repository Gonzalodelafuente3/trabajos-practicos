package situacion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Curso {
    private String nombreCurso;
    private int codigo;
    private List <Estudiante> listaDeEstudiantes = new ArrayList<Estudiante>();
    private List <Profesor> listaDeProfesor = new ArrayList<Profesor>();

    public Curso(String nombre,int codigo){
        this.nombreCurso=nombre;
        this.codigo=codigo;
        
    }

    
    public String getNombreDelCurso(){
        return nombreCurso;
    }
    public int getCodigoCurso(){
        return codigo;
    }
    public void setNombreCurso(String nombre){
        this.nombreCurso=nombre;
    }
    public void setCodigoCurso(int codigo){
        this.codigo=codigo;
    }
    public void resetearNotas(){
        for(Estudiante estudiante : listaDeEstudiantes){
            estudiante.setCalificacion(0);
        }
    }
    public void agregarEstudiante(Estudiante estudiante){
        for(Estudiante aux: listaDeEstudiantes){
            if(aux.coincideMatricula(estudiante.getMatricula())){
                throw new EstudianteRepetidoException("No se puede agregar al Estudiante porque ya Existe");
                
            }
        }
        
        listaDeEstudiantes.add(estudiante);

    }
    public int cantidadDeEstudiantesInscriptos(){
        int contador=0;
        for(Estudiante aux:listaDeEstudiantes){
            contador++;
        }
        return contador;

    }
    public boolean existeEstudianteConNotaDiez(){
        boolean respuesta=false;

        for(Estudiante aux : listaDeEstudiantes){
            if(aux.getCalificacion()==10){
                return true;
            }
        }
        return respuesta;
          

    }
    public List<Estudiante> estudiantes(){
        return listaDeEstudiantes;
    }
    public ArrayList<Estudiante> estudiantesAprobados(){
        ArrayList<Estudiante> alumnosAprobados = new ArrayList<Estudiante>();
        for (Estudiante aux:listaDeEstudiantes){
            if(aux.getCalificacion()>=4){
                alumnosAprobados.add(aux);

            }
        }
        return alumnosAprobados;
    }
    public int cantidadDeAlumnosAprobados(){
        int contador=0;
        for(Estudiante aux:listaDeEstudiantes){
            if(aux.getCalificacion()>=4){
                contador++;
            }
        }
        return contador;
    }
    public double porcentajeDeAprobados(){
        double porcentajeDeAprobados;
        porcentajeDeAprobados= (cantidadDeAlumnosAprobados()*100)/cantidadDeEstudiantesInscriptos();
        return porcentajeDeAprobados;

    }
    public double promedioDeCalificaciones(){
        double acumulador =0;
        for(Estudiante aux:listaDeEstudiantes){
            acumulador= acumulador + aux.getCalificacion();
        }
        double promedio = acumulador / cantidadDeEstudiantesInscriptos();
        return promedio;

    }
    public ArrayList<String> ciudadesExceptoCapital(){

        ArrayList<String> departamentoDeLosEstudiantes = new ArrayList<String>();
        ArrayList<String> deparamentoSinRepetir = new ArrayList<String>();
        String variable="CAPITAL";
        for(Estudiante aux: listaDeEstudiantes){
            if(!aux.getDepartamento().equals(variable)){
                departamentoDeLosEstudiantes.add(aux.getDepartamento());

            }
            
        }
        Set<String> hashSet = new HashSet<String>(departamentoDeLosEstudiantes);
        deparamentoSinRepetir.addAll(hashSet);
        return deparamentoSinRepetir;



    }
    public boolean unDesastre(){
        for(Estudiante aux:listaDeEstudiantes){
            if(aux.getCalificacion()>=4){
                return false;
            }
        }
        return true;
    }
    public boolean existeEstudiante(int matricula){
        for(Estudiante aux:listaDeEstudiantes){
            if(aux.getMatricula()==matricula){
                return true;
            }
        }
        return false;

    }
    
    public boolean existeEstudianteLlamado(String apellido){
        for(Estudiante aux:listaDeEstudiantes){
            if(aux.getApellido()==apellido){
                return true;
            }
        }
        return false;

    }
    
    public ArrayList<Estudiante> estudianteDelInteriorProvincial(){
        ArrayList<Estudiante> alumnosDelInterior = new ArrayList<Estudiante>();
        String variable="CAPITAL";
        for (Estudiante aux:listaDeEstudiantes){
            if(!aux.getDepartamento().equals(variable)){
                alumnosDelInterior.add(aux);

            }
        }
        return alumnosDelInterior;
    }
    public Estudiante buscarEstudiante(int matricula){
        Estudiante estudianteEncontrado=null;
        for(Estudiante aux:listaDeEstudiantes){
            if(aux.getMatricula()==matricula){
                estudianteEncontrado=aux;
                break;
                
            }
        }
        if(estudianteEncontrado==null){
            throw new EstudianteInexistenteException("Estudiante no Encontrado");
        }
        return estudianteEncontrado;
    }
    public void eliminarAlumnos(int matricula){
        Estudiante estudianteEncontrado= buscarEstudiante(matricula);
        listaDeEstudiantes.remove(estudianteEncontrado);
    }
    public void agregarProfesor(Profesor profesor ){
        for(Profesor aux: listaDeProfesor){
            if(aux.coincideDni(profesor.getDni())){
                throw new ProfesorRepetidoException("No se puede agregar al Profesor porque ya Existe");
                
            }
        }
        listaDeProfesor.add(profesor);

    }
    public int cantidadDeProfesor(){
        int contador=0;
        for(Profesor aux:listaDeProfesor){
            contador++;
        }
        return contador;

    }
    public Profesor buscarProfesor(String apellido, String nombre){
        Profesor profesorEncontrado=null;
        for(Profesor aux:listaDeProfesor){
            if(aux.getApellido()==apellido && aux.getNombre()==nombre){
                profesorEncontrado=aux;
                break;
            }
        }
        if(profesorEncontrado==null){
            throw new ProfesorInexistenteException("El Profesor no existe");
        }
        return profesorEncontrado;
        


        
    }
    public void eliminarProfesor(String apellido, String nombre){
        Profesor profesorEncontrado= buscarProfesor(apellido,nombre);
        listaDeProfesor.remove(profesorEncontrado);
    }
    public List<Profesor> profesoresDelCurso(){
        return listaDeProfesor;
    }
    public List<Estudiante> ordenarPorApellido(){
        Collections.sort(listaDeEstudiantes);
        return listaDeEstudiantes;
    }
    public List<Estudiante> ordenarPorNota(){
        Collections.sort(listaDeEstudiantes, new OrdenarPorNota());
        return listaDeEstudiantes;
    }
    public List<Estudiante> ordenarPorNotaEstudiantesAprobados(){
        List<Estudiante> estudiantesAprobados = estudiantesAprobados();
        Collections.sort(estudiantesAprobados, new OrdenarPorNota());
        return estudiantesAprobados;


    }
     public List<Estudiante> ordenarPorApellidoYDepartamento(){
        Collections.sort(listaDeEstudiantes, new OrdenarPorApellidoyDepartamento());
        return listaDeEstudiantes;

    } 



    


}
