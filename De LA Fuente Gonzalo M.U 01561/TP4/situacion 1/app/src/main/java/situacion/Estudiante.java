package situacion;

import java.util.Comparator;

public class Estudiante extends Persona implements Comparable {
    
    private Integer matricula;
    private double calificacion;
    private String departamentoProvincial;

    public Estudiante(String nombre,String apellido,int edad,int dni,String departamento,int matricula,int nota){
 
        super(nombre,apellido,edad,dni);
        this.calificacion=nota;
        this.departamentoProvincial=departamento.toUpperCase();

        this.matricula=matricula;
    }
    public void setCalificacion(int nota){
        this.calificacion=nota;
    }
 
    public double getCalificacion(){
        return calificacion;
    }
    public Integer getMatricula(){
        return matricula;
    }

    public String getDepartamento(){
        return departamentoProvincial;
    }

    
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "Apellido: "+getApellido()+ "\tNombre: "+ getNombre()+"\tMatricula: "+matricula+"\tNota: "+ calificacion+"\tDepartamento: "+departamentoProvincial+"\n";
    }
    
    public boolean coincideMatricula(int matricula){
        return getMatricula().equals(matricula);
    }
    @Override
    public int compareTo(Object o) {
        Estudiante estudiante = (Estudiante) o ;
        // TODO Auto-generated method stub
        return this.getApellido().compareTo(estudiante.getApellido());//comparar por apellido
    }

    
    

    

  
 
  
    


}