package situacion;

import java.util.Comparator;

public class OrdenarPorApellidoyDepartamento implements Comparator{



    @Override
    public int compare(Object o1, Object o2) {
        Estudiante e1 = (Estudiante)o1;
        Estudiante e2 = (Estudiante)o2;
        // TODO Auto-generated method stub
        int respuesta;
        respuesta = e1.getApellido().compareTo(e2.getApellido());
        if(respuesta==0){
            respuesta = e1.getDepartamento().compareTo(e2.getDepartamento());
        }
        return respuesta;
    }
    
}