package situacion;

public class ProfesorRepetidoException extends RuntimeException{
    public ProfesorRepetidoException(String mensaje){
        super(mensaje);
    }

}
