package situacion;

public class EstudianteRepetidoException extends RuntimeException {
    public EstudianteRepetidoException(String mensaje){
        super(mensaje);
    }
    
}
