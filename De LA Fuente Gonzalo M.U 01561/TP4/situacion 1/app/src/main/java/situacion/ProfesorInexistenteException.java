package situacion;

public class ProfesorInexistenteException extends RuntimeException{
    public ProfesorInexistenteException(String mensaje){
        super(mensaje);
    }
}
