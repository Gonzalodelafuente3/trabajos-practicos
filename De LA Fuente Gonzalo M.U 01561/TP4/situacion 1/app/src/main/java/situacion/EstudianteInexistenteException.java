package situacion;

public class EstudianteInexistenteException extends RuntimeException{
    public EstudianteInexistenteException(String mensaje){
        super(mensaje);
    }
}
