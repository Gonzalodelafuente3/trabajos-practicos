package situacion;

import java.util.Comparator;

public class OrdenarPorNota implements Comparator{

    @Override
    public int compare(Object o1, Object o2) {
        // TODO Auto-generated method stub
        Estudiante e1 = (Estudiante) o1;
        Estudiante e2 = (Estudiante) o2;
        //comparar por edad
        int respuesta = -1;
        if(e1.getCalificacion() == e2.getCalificacion()){
            respuesta = (int) (e1.getCalificacion() - e2.getCalificacion());
        }else{
            respuesta = (int) (e2.getCalificacion() - e1.getCalificacion());
        }

        return respuesta;
    }
    
}
