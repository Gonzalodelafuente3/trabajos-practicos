
package situacion;

public class Profesor extends Persona {
    private String titulo;

    public Profesor(String nombre,String apellido,int edad,int dni,String titulo){
        super(nombre,apellido,edad,dni);
        this.titulo=titulo;

        
    }
    public String getTitulo(){
        return titulo;
        
    }
    
    
}

