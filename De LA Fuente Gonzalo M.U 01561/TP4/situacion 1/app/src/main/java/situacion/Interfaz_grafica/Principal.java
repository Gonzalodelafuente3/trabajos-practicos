/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package situacion.Interfaz_grafica;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import situacion.Curso;
import situacion.Estudiante;
import situacion.Profesor;

/**
 *
 * @author Usuario
 */

public class Principal extends javax.swing.JFrame {
    public  Curso curso;
    

    /**
     * Creates new form Principal
     */
    public Principal() {
        
        
        initComponents();
        setLocationRelativeTo(null);
        curso = new Curso("sin nombre",0);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jMenuBarListaProfesor = new javax.swing.JMenuBar();
        jMenuRegistrar = new javax.swing.JMenu();
        jMenuItemDatoDelCurso = new javax.swing.JMenuItem();
        jMenuItemRegistroEstudiante = new javax.swing.JMenuItem();
        jMenuItemRegistroProfesor = new javax.swing.JMenuItem();
        jMenuCurso = new javax.swing.JMenu();
        jMenuItemDatosDelCurso = new javax.swing.JMenuItem();
        EliminarEstudiante = new javax.swing.JMenuItem();
        buscarEstudiante = new javax.swing.JMenuItem();
        jMenuEstudiante = new javax.swing.JMenu();
        jMenuItemListadoEstudiante = new javax.swing.JMenuItem();
        CantidadEstudiantes = new javax.swing.JMenuItem();
        jMenuItemPromedioDeCalificaciones = new javax.swing.JMenuItem();
        jMenuItemDesastre = new javax.swing.JMenuItem();
        jMenuItemNota10 = new javax.swing.JMenuItem();
        jMenuProfesor = new javax.swing.JMenu();
        jMenuItemListadoDeProfesor = new javax.swing.JMenuItem();
        cantidadProfesores = new javax.swing.JMenuItem();
        jMenuSalir = new javax.swing.JMenu();
        jMenuItemSalirDelPrograma = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jPanel5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CURSO-ONLINE.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(121, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(104, 104, 104))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel5, java.awt.BorderLayout.CENTER);

        jMenuRegistrar.setText("Registrar");

        jMenuItemDatoDelCurso.setText("Cargar Datos Del Curso");
        jMenuItemDatoDelCurso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemDatoDelCursoActionPerformed(evt);
            }
        });
        jMenuRegistrar.add(jMenuItemDatoDelCurso);

        jMenuItemRegistroEstudiante.setText("Registrar Estudiante");
        jMenuItemRegistroEstudiante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRegistroEstudianteActionPerformed(evt);
            }
        });
        jMenuRegistrar.add(jMenuItemRegistroEstudiante);

        jMenuItemRegistroProfesor.setText("Registrar Profesor");
        jMenuItemRegistroProfesor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRegistroProfesorActionPerformed(evt);
            }
        });
        jMenuRegistrar.add(jMenuItemRegistroProfesor);

        jMenuBarListaProfesor.add(jMenuRegistrar);

        jMenuCurso.setText("Curso");

        jMenuItemDatosDelCurso.setText("Mostrar Datos Del Curso");
        jMenuItemDatosDelCurso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemDatosDelCursoActionPerformed(evt);
            }
        });
        jMenuCurso.add(jMenuItemDatosDelCurso);

        EliminarEstudiante.setText("Eliminar Estudiante");
        EliminarEstudiante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminarEstudianteActionPerformed(evt);
            }
        });
        jMenuCurso.add(EliminarEstudiante);

        buscarEstudiante.setText("Buscar Estudiante");
        buscarEstudiante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscarEstudianteActionPerformed(evt);
            }
        });
        jMenuCurso.add(buscarEstudiante);

        jMenuBarListaProfesor.add(jMenuCurso);

        jMenuEstudiante.setText("Estudiante");

        jMenuItemListadoEstudiante.setText("Listado");
        jMenuItemListadoEstudiante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemListadoEstudianteActionPerformed(evt);
            }
        });
        jMenuEstudiante.add(jMenuItemListadoEstudiante);

        CantidadEstudiantes.setText("Cantidad de Estudiantes");
        CantidadEstudiantes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CantidadEstudiantesActionPerformed(evt);
            }
        });
        jMenuEstudiante.add(CantidadEstudiantes);

        jMenuItemPromedioDeCalificaciones.setText("Promedio de Notas");
        jMenuItemPromedioDeCalificaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPromedioDeCalificacionesActionPerformed(evt);
            }
        });
        jMenuEstudiante.add(jMenuItemPromedioDeCalificaciones);

        jMenuItemDesastre.setText("�El Curso fue un Desastre?");
        jMenuItemDesastre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemDesastreActionPerformed(evt);
            }
        });
        jMenuEstudiante.add(jMenuItemDesastre);

        jMenuItemNota10.setText("�Algun Estudiante Saco Nota 10?");
        jMenuItemNota10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemNota10ActionPerformed(evt);
            }
        });
        jMenuEstudiante.add(jMenuItemNota10);

        jMenuBarListaProfesor.add(jMenuEstudiante);

        jMenuProfesor.setText("Profesor");

        jMenuItemListadoDeProfesor.setText("Listado");
        jMenuItemListadoDeProfesor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemListadoDeProfesorActionPerformed(evt);
            }
        });
        jMenuProfesor.add(jMenuItemListadoDeProfesor);

        cantidadProfesores.setText("Cantidad de Profesores ");
        cantidadProfesores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantidadProfesoresActionPerformed(evt);
            }
        });
        jMenuProfesor.add(cantidadProfesores);

        jMenuBarListaProfesor.add(jMenuProfesor);

        jMenuSalir.setText("Salir");
        jMenuSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuSalirActionPerformed(evt);
            }
        });

        jMenuItemSalirDelPrograma.setText("Salir del Prgrama");
        jMenuItemSalirDelPrograma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSalirDelProgramaActionPerformed(evt);
            }
        });
        jMenuSalir.add(jMenuItemSalirDelPrograma);

        jMenuBarListaProfesor.add(jMenuSalir);

        setJMenuBar(jMenuBarListaProfesor);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemDatosDelCursoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemDatosDelCursoActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Nombre de Curso:"+curso.getNombreDelCurso()+"\nCodigo de Curso: "+curso.getCodigoCurso());
        
        
        
        
    }//GEN-LAST:event_jMenuItemDatosDelCursoActionPerformed

    private void jMenuSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuSalirActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jMenuSalirActionPerformed

    private void jMenuItemSalirDelProgramaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSalirDelProgramaActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jMenuItemSalirDelProgramaActionPerformed

    private void jMenuItemDatoDelCursoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemDatoDelCursoActionPerformed
        // TODO add your handling code here:
        DatosDelCurso datos= new DatosDelCurso(this,true,curso);
        datos.setLocationRelativeTo(this);
        datos.setVisible(true);
    }//GEN-LAST:event_jMenuItemDatoDelCursoActionPerformed

    private void jMenuItemRegistroEstudianteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRegistroEstudianteActionPerformed
        // TODO add your handling code here:
        AgregarEstudiante nuevoEstudiante = new AgregarEstudiante(this,true,curso);
        nuevoEstudiante.setLocationRelativeTo(this);
        nuevoEstudiante.setVisible(true);
        
    }//GEN-LAST:event_jMenuItemRegistroEstudianteActionPerformed

    private void jMenuItemListadoEstudianteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemListadoEstudianteActionPerformed
        // TODO add your handling code here:
        ListaEstudiantes listaEstudiante = new ListaEstudiantes(this, true, curso);
        listaEstudiante.setLocationRelativeTo(this);
        listaEstudiante.setVisible(true);
        
    }//GEN-LAST:event_jMenuItemListadoEstudianteActionPerformed

    private void jMenuItemRegistroProfesorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRegistroProfesorActionPerformed
        // TODO add your handling code here:
        AgregarProfesor agregarProfesor = new AgregarProfesor(this,true,curso);
        agregarProfesor.setLocationRelativeTo(this);
        agregarProfesor.setVisible(true);
        
    }//GEN-LAST:event_jMenuItemRegistroProfesorActionPerformed

    private void jMenuItemListadoDeProfesorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemListadoDeProfesorActionPerformed
        // TODO add your handling code here:
        ListaProfesor listaProfesor = new ListaProfesor(this,true,curso);
        listaProfesor.setLocationRelativeTo(this);
        listaProfesor.setVisible(true);
    }//GEN-LAST:event_jMenuItemListadoDeProfesorActionPerformed

    private void CantidadEstudiantesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CantidadEstudiantesActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null,"Cantidad de Estudiantes: "+curso.cantidadDeEstudiantesInscriptos());
    }//GEN-LAST:event_CantidadEstudiantesActionPerformed

    private void cantidadProfesoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantidadProfesoresActionPerformed
        // TODO add your handling code here:
      
        JOptionPane.showMessageDialog(null,"Cantidad de Profesoes: "+curso.cantidadDeProfesor());
    }//GEN-LAST:event_cantidadProfesoresActionPerformed

    private void jMenuItemPromedioDeCalificacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPromedioDeCalificacionesActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null,"Cantidad de Profesoes: "+curso.promedioDeCalificaciones());
    }//GEN-LAST:event_jMenuItemPromedioDeCalificacionesActionPerformed

    private void jMenuItemDesastreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemDesastreActionPerformed
        // TODO add your handling code here:
        if(curso.unDesastre()== true){
            JOptionPane.showMessageDialog(null,"Si fue un Desastre");
        }else{
            JOptionPane.showMessageDialog(null,"No fue un Desastre");
        }
    }//GEN-LAST:event_jMenuItemDesastreActionPerformed

    private void jMenuItemNota10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemNota10ActionPerformed
        // TODO add your handling code here:
        if(curso.existeEstudianteConNotaDiez()== true){
            JOptionPane.showMessageDialog(null,"Existe Estudiante/es con Nota 10");
        }else{
            JOptionPane.showMessageDialog(null,"Ningun Estudiante Obtuvo 10");
        }
    }//GEN-LAST:event_jMenuItemNota10ActionPerformed

    private void EliminarEstudianteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminarEstudianteActionPerformed
        // TODO add your handling code here:
        EliminarEstudiante eliminado = new EliminarEstudiante(this,true,curso);
        eliminado.setLocationRelativeTo(this);
        eliminado.setVisible(true);
    }//GEN-LAST:event_EliminarEstudianteActionPerformed

    private void buscarEstudianteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscarEstudianteActionPerformed
        // TODO add your handling code here:
        BuscarEstudiante buscar = new BuscarEstudiante(this,true,curso);
        buscar.setLocationRelativeTo(this);
        buscar.setVisible(true);
    }//GEN-LAST:event_buscarEstudianteActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem CantidadEstudiantes;
    private javax.swing.JMenuItem EliminarEstudiante;
    private javax.swing.JMenuItem buscarEstudiante;
    private javax.swing.JMenuItem cantidadProfesores;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBarListaProfesor;
    private javax.swing.JMenu jMenuCurso;
    private javax.swing.JMenu jMenuEstudiante;
    private javax.swing.JMenuItem jMenuItemDatoDelCurso;
    private javax.swing.JMenuItem jMenuItemDatosDelCurso;
    private javax.swing.JMenuItem jMenuItemDesastre;
    private javax.swing.JMenuItem jMenuItemListadoDeProfesor;
    private javax.swing.JMenuItem jMenuItemListadoEstudiante;
    private javax.swing.JMenuItem jMenuItemNota10;
    private javax.swing.JMenuItem jMenuItemPromedioDeCalificaciones;
    private javax.swing.JMenuItem jMenuItemRegistroEstudiante;
    private javax.swing.JMenuItem jMenuItemRegistroProfesor;
    private javax.swing.JMenuItem jMenuItemSalirDelPrograma;
    private javax.swing.JMenu jMenuProfesor;
    private javax.swing.JMenu jMenuRegistrar;
    private javax.swing.JMenu jMenuSalir;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel5;
    // End of variables declaration//GEN-END:variables

    private void DatosDelCurso() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
