package Situacion3_TP2;

public class Vehiculo {
    private String marca;
    private String modelo;
    private String patente;
    private double kilometraje;
    
    public Vehiculo(String marca, String modelo, String patente, double kilometraje){
        this.marca = marca;
        this.modelo = modelo;
        this.patente = patente;
        this.kilometraje = kilometraje;
        
    }
    public String getMarca(){
        return marca;

    }
    public String getModelo(){
        return modelo;
        
    }
    public String getPatente(){
        return patente;
        
    }
    public double getKilometraje(){
        return kilometraje;
        
    }
}
