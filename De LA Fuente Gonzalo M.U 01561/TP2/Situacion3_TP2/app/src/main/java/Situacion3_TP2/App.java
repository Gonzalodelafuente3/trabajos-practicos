/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package Situacion3_TP2;

public class App {
    public String getGreeting() {
        return "Hello World!";
    }

    public static void main(String[] args) {

        Taller taller=new Taller("Taller El Mecanico", "Barrio norte","154123693");

        DatosPropietario persona1 = new DatosPropietario("luis", "perez", "barrio norte", "154333333","43123333");
        Vehiculo auto1 = new Vehiculo("Ford", "Fiesta", "AAC 333", 35000);
        Ficha ficha1 = new Ficha("aire accondicionado defectuoso","reparacion del aire acondicionado","Compresor Aire Acondicionado", 80300);
        System.out.println("***"+ taller.getNombre() +"***");
        System.out.println("direccion:"+ taller.getDireccion());
        System.out.println("telefono: "+ taller.getTelefono());
        
        
        System.out.println("**********FICHA*********\n");
        System.out.println("Nombre:                 "+ persona1.getNombre());
        System.out.println("Apellido:               "+ persona1.getApellido());
        System.out.println("Direccion:              "+ persona1.getDireccion());
        System.out.println("Tel:                    "+ persona1.getTelefono());
        System.out.println("Documento:              "+ persona1.getDni());
        System.out.println("Marca del auto:         "+ auto1.getMarca());
        System.out.println("Modelo del auto:        "+ auto1.getModelo());
        System.out.println("Patente del auto:       "+ auto1.getPatente());
        System.out.println("Kilometraje :           "+ auto1.getKilometraje() );
        System.out.println("Motivo :                "+ ficha1.getMotivoCualLoTrae());
        System.out.println("Respuestos utilizados:  "+ ficha1.getRespuestoUtilizados());
        System.out.println("Tarea efectuada:        "+ ficha1.getTareaEfectuada());
        System.out.println("Precio de mano de obra: "+ ficha1.getManoDeObra());
    }
}


