package Situacion3_TP2;

public class Ficha {
    
    private String motivoCualLoTrae;
    private String tareaEfectuada;
    private String respuestoUtilizados;
    private float manoDeObra;
    
    public Ficha(String motivo,String tareaEfectuada,String respuestos, float manoDeObra){
        
        this.motivoCualLoTrae= motivo;
        this.tareaEfectuada= tareaEfectuada;
        this.respuestoUtilizados= respuestos;
        this.manoDeObra= manoDeObra;
    }
    
   
    public String getMotivoCualLoTrae(){
        return motivoCualLoTrae;
    }
    public String getRespuestoUtilizados(){
        return respuestoUtilizados;
    }
    public String getTareaEfectuada(){
        return tareaEfectuada;
    }
    public float getManoDeObra(){
        return manoDeObra;
    }
}
