package Situacion3_TP2;

public class DatosPropietario {
    private String nombre;
    private String apellido;
    private String direccion;
    private String telefono ;
    private String dni;

    public DatosPropietario(){
        this.nombre = "sin nombre";
        this.apellido = "sin apellido";
        this.direccion = "sin direccion";
        this.telefono = "sin telefono";
        this.dni = "sin dni";
    }
    public DatosPropietario(String nombre, String apellido, String direccion, String telefono,String dni){
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.telefono = telefono;
        this.dni = dni;
    }
    public String getNombre(){
        return nombre;
    }
    public String getApellido(){
        return apellido;
    }
    public String getDireccion(){
        return direccion;
    }
    public String getTelefono(){
        return telefono;
    }
    public String getDni(){
        return dni;
    }


}
