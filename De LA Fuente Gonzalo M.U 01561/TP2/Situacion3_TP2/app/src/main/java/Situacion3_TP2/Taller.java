package Situacion3_TP2;

public class Taller {
    private String nombre;
    private String direccion;
    private String telefono;

    public Taller(String nombre,String direccion,String tel){
        this.nombre=nombre;
        this.direccion=direccion;
        this.telefono= tel;
    }
   
    public String getNombre(){
        return nombre;
    }
    public String getDireccion(){
        return direccion;
    }
    public String getTelefono(){
        return telefono;
    }

    
}
