package situacion1.TP2;

public class Contenido {
    private float lipido;
    private float hidratosCarbono;
    private float proteinas;
    private String vitaminas;


    public Contenido(){
        this.lipido = 0;
        this.hidratosCarbono = 0;
        this.proteinas = 0;
        this.vitaminas = "sin vitaminas";
    }
    
    public Contenido(float lipido,float hidratos,float proteinas,String vitaminas){
        this.lipido = lipido;
        this.hidratosCarbono = hidratos;
        this.proteinas = proteinas;
        this.vitaminas = vitaminas;
    }
    
    public float getLipido(){
        return lipido;
    }
    public float getProteina(){
        return proteinas;
    }
    public float getHidratosCarbono(){
        return hidratosCarbono;
    }
    public String getVitaminas(){
        return vitaminas;
    }


}

