package situacion1.TP2;

import situacion1.TP2.Alimento;
import situacion1.TP2.Contenido;

public class TipoDeAlimento   {
    private float totalDeMacroNutrientes;
    private boolean respuesta;
    public boolean alimentoDietetico(Alimento alimento){

        
        totalDeMacroNutrientes = alimento.getLipido() + alimento.getHidratosCarbono() + alimento.getProteina();
        
        double operacion= totalDeMacroNutrientes * 0.20;
       
        String cadena=alimento.getVitaminas();
       
        if((alimento.getLipido()< operacion && alimento.getVitaminas()==cadena) ){
            
            
            return respuesta= true;

        }else{
            return respuesta= false;
        }
    }
    public boolean alimentoParaDeportistas(Alimento alimento){
        
        int contador=0;

        totalDeMacroNutrientes= alimento.getLipido() + alimento.getHidratosCarbono() + alimento.getProteina();
        if ((totalDeMacroNutrientes * 0.10)<alimento.getProteina() && alimento.getProteina()<(totalDeMacroNutrientes*0.15)){
            contador= contador +1;
        }else if((totalDeMacroNutrientes * 0.55)<alimento.getHidratosCarbono() && alimento.getHidratosCarbono()<(totalDeMacroNutrientes*0.65)){
            contador= contador +1;
        }else if ((totalDeMacroNutrientes * 0.30)<alimento.getLipido() && alimento.getLipido()<(totalDeMacroNutrientes*0.35)){
            contador= contador +1;
        }
        if(contador==3){
            return respuesta= true;
            
            
            
        }else{
            return respuesta=false;
            
        }

    }
    public double valorEnergetico(Alimento alimento){
        double valorEnergetico= (alimento.getLipido()*9.4) + (alimento.getHidratosCarbono()*4.1)+(alimento.getProteina()*5.3);
        return valorEnergetico;
    }


    
}
