package situacion1.TP2;

public class Alimento extends Contenido{
    private String descripcion;
    private String nombre;
    private String origenAnimal;

    public Alimento(float lipido,float hidratos,float proteinas,String vitaminas,String nombre, String descripcion,String origenAnimal){
        super(lipido, hidratos, proteinas, vitaminas);
        this.nombre= nombre;
        this.descripcion = descripcion;
        this.origenAnimal = origenAnimal;
    
    }
    public String getNombre(){
        return nombre;
    }
    
    public String getDescipcion(){
        return descripcion;
    }
    
    public String getOrigenAnimal(){
        return origenAnimal;
    }
    
}

