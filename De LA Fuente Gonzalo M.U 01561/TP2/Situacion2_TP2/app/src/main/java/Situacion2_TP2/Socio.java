package Situacion2_TP2;

import javax.lang.model.util.ElementScanner6;

public class Socio {
    private String nombre;
    private String apellido;
    private long documento;
    private int claseDeSocio;

    public Socio(){
        this.nombre = "Sin nombre";
        this.apellido = "Sin apellido";
        this.documento = 0;
    }
    public Socio(String nombre,String apellido, long dni, int clase){
        this.nombre = nombre;
        this.apellido = apellido;
        this.documento = dni;
        this.claseDeSocio=clase;
    }
    public String  getNombre(){
        return nombre;

    }
    public String getApellido(){
        return apellido;
        
    }
    public long getDocumento(){
        return documento;
    }
    public int numeroDeSocio(){
        return claseDeSocio;
    }
    public String tipoDeSocio(){
        if(numeroDeSocio()==1){
            return "Socio comun";
        }else if(numeroDeSocio()==2){
            return "Socio vitalicio";

        }else{
            return "Socio Adherente";
        }
    }
}

