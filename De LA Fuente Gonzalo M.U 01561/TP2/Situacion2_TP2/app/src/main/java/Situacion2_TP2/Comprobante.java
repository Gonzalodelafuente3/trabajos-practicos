package Situacion2_TP2;

public class Comprobante extends Socio {
    
    private float importe;
    private int cuotasPagadas;
    public Comprobante(String nombre,String apellido,long dni,int clase,int cuota){
        super(nombre, apellido, dni,clase);
        this.cuotasPagadas=cuota;

    }
    public float calculoDeImporte(){
        switch (numeroDeSocio()) {
            case 1:
                this.importe=1500;
                break;
        
            case 2:
                this.importe=0;
                
                break;
            case 3:
                this.importe=(1500*50)/100;
                break;
        }
        return importe;
    }
    public int getCuotasPagadas(){
        return cuotasPagadas;
    }

}
