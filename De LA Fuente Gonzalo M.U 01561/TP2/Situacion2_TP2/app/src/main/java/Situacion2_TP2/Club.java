package Situacion2_TP2;

public class Club {
    private String nombre;
    private String direccion;
    private String telefono;
    
    public Club(String nombre,String direccion,String telefono){
        this.nombre=nombre;
        this.direccion=direccion;
        this.telefono=telefono;
    }
    public String getNombre(){
        return nombre;
    }
    public String getDireccion(){
        return direccion;
    }

    public String getTelefono(){
        return telefono;
    }

}

